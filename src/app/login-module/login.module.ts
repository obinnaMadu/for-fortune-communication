import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnauthenticatedHeaderComponent } from './unauthenticated-header/unauthenticated-header.component';
import { LoginComponent } from './login/login.component';
import { BaseComponent } from './base/base.component';
import {RouterModule, Routes} from '@angular/router';
import {MatButtonModule, MatCardModule, MatIconModule, MatInputModule, MatToolbarModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../footer-module/footer-module.module';

const routes: Routes = [
  { path: '', component: BaseComponent, children: [
    { path: '', component: LoginComponent}
]}];

@NgModule({
  declarations: [UnauthenticatedHeaderComponent, LoginComponent, BaseComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FooterModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule
  ]
})
export class LoginModule { }
