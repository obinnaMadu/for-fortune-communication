import { Component, OnInit } from '@angular/core';
import {faArrowLeft, faCoffee, faEnvelope, faUser} from '@fortawesome/free-solid-svg-icons';
import {CreateConversationComponent} from '../create-conversation/create-conversation.component';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-view-all-messages',
  templateUrl: './view-all-messages.component.html',
  styleUrls: ['./view-all-messages.component.css']
})
export class ViewAllMessagesComponent implements OnInit {

  displayedColumns: string[] = ['position', 'category', 'subject', 'from-to', 'designation', 'created', 'notification', 'symbol'];
  dataSource = ELEMENT_DATA;
  faEnvelope = faEnvelope;
  faArrowLeft = faArrowLeft;

  constructor(public dialog: MatDialog,
              private router: Router) { }

  ngOnInit() {
  }

  openCreateConversation() {
    const dialogRef = this.dialog.open(CreateConversationComponent, {
      width: '550px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  redirect() {
    this.router.navigate(['/main/chat']);
  }
}
