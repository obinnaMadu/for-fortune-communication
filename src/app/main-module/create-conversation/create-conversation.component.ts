import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-conversation',
  templateUrl: './create-conversation.component.html',
  styleUrls: ['./create-conversation.component.css']
})
export class CreateConversationComponent implements OnInit {

  createConversation: FormGroup;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(private fb: FormBuilder) { }
  ngOnInit() {
    this.createConversation = this.fb.group({
        subject : new FormControl(null, Validators.required),
        message : new FormControl(null, Validators.required),
        recipients: new FormControl(null, Validators.required),
        designations: new FormControl(null, Validators.required),
        upload: new FormControl(null, Validators.required)
    });
  }

}
