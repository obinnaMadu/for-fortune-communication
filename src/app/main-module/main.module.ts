import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticatedHeaderComponent} from './authenticated-header/authenticated-header.component';
import {ChatComponent} from './chat/chat.component';
import {ViewAllMessagesComponent} from './view-all-messages/view-all-messages.component';
import {MainComponent} from './main/main.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../footer-module/footer-module.module';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {TopNavBarComponent} from './authenticated-header/top-nav-bar/top-nav-bar.component';
import {BottomNavBarComponent} from './authenticated-header/bottom-nav-bar/bottom-nav-bar.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CreateConversationComponent} from './create-conversation/create-conversation.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: '', redirectTo: 'inbox', pathMatch: 'full'},
      { path: 'inbox', component: ViewAllMessagesComponent},
      { path: 'chat', component: ChatComponent}
    ]
  },
];

@NgModule({
  declarations: [
    AuthenticatedHeaderComponent,
    ChatComponent,
    ViewAllMessagesComponent,
    MainComponent,
    TopNavBarComponent,
    BottomNavBarComponent,
    CreateConversationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FooterModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    FontAwesomeModule,
    MatDialogModule,
    MatOptionModule,
    MatInputModule,
    MatSelectModule,
    RouterModule.forChild(routes)
  ], entryComponents: [CreateConversationComponent]
})
export class MainModule {
}
