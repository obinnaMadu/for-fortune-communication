import { Component, OnInit } from '@angular/core';
import {faEnvelope, faPowerOff, faThLarge, faUser} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bottom-nav-bar',
  templateUrl: './bottom-nav-bar.component.html',
  styleUrls: ['./bottom-nav-bar.component.css']
})
export class BottomNavBarComponent implements OnInit {

  faPowerOff = faPowerOff;
  fathLarge = faThLarge ;
  faUser = faUser;
  constructor() { }

  ngOnInit() {
  }

}
