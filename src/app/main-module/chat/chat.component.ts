import { Component, OnInit } from '@angular/core';
import {faCheck, faCheckCircle} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  escalationMessages: any[] = [];
  username = 'ccccc';
  faCheck = faCheckCircle;
  constructor() { }

  ngOnInit() {
    this.escalationMessages.push({
      createdBy: {
        username: 'ccccc',
        gender: 'Male'
      },
      content: 'xxxccvcvcvcvcvcvcvcvcc',
      dateCreated: '2019-02-06'
    });

    this.escalationMessages.push({
      createdBy: {
        username: 'wwccccc',
        gender: 'Female'
      },
      content: 'xbxxccvcvcvcvcvcvcvcvcc',
      dateCreated: '2019-02-06'
    });
  }

}
